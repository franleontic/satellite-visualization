# ---------------------------------------
# Final Project A for UB DMS 423 - Fall 14
# Real-time Satellite Visualization
# by Xiaoyu Tai
#
# Input Data type: TLS(Two-line element set)
# Can be found at http://www.celestrak.com/NORAD/elements/
#
# Tweak from global.py from Dr.Dave Pape
# https://gist.github.com/davepape/7324958
#
# Hi-Res Map is from NASA:
# http://earthobservatory.nasa.gov/blogs/elegantfigures/files/2011/10/land_shallow_topo_2011_8192.jpg
# ---------------------------------------

import ephem, datetime, urllib.request, urllib.error, urllib.parse, json, sys
import pyglet
import platform
if platform.system() != "Windows":
	pyglet.options['headless'] = True
from pyglet.gl import *
from PIL import Image
from ctypes import c_uint, byref
from math import *
import os

pathcurrent = os.path.dirname(os.path.realpath(__file__))

lat = 45.81
lon = 15.96
height = 30000
xangle = 0
yangle = 0


# OPTIONS:
#   num      = satellites set number is NOAA weather
#   rang     = satellites trace line points
#   interval = satellites trace line points interval
#   online   = if fetch data from http://www.celestrak.com/NORAD/elements/
# Local data update: 2014-12-13 07:38 UTC




lat = float(sys.argv[1])
lon = float(sys.argv[2])
height = (float(sys.argv[3]) / 1000)
xangle = float(sys.argv[4])
yangle = float(sys.argv[5])


gyration = []
zoom = 0
gyration.append([lat, lon])  # latitude, longitude
gyration.append([xangle, yangle, 0])  # x-axis, y-axis, z-axis


def init():
	glEnable(GL_DEPTH_TEST)

	global earth_texture, earth_vlists
	earth_texture = pyglet.image.load(pathcurrent + '/assets/8192.jpg').get_texture()
	earth_vlists, step, radius = [], 9, 6.37815
	for lat in range(-90,90,step):
		verts, texc = [], []
		for lon in range(-180,181,step):
			x1 = -cos(radians(lat)) * cos(radians(lon)) * radius
			y1 = sin(radians(lat)) * radius
			z1 = cos(radians(lat)) * sin(radians(lon)) * radius
			s1 = (lon + 180) / 360.0
			t1 = (lat + 90) / 180.0
			x2 = -cos(radians((lat+step))) * cos(radians(lon)) * radius
			y2 = sin(radians((lat+step))) * radius
			z2 = cos(radians((lat+step))) * sin(radians(lon)) * radius
			s2 = (lon + 180) / 360.0
			t2 = ((lat + step) + 90) / 180.0
			verts += [x1,y1,z1, x2,y2,z2]
			texc += [s1,t1, s2,t2]
		earth_vlists.append(pyglet.graphics.vertex_list(int(len(verts)/3), ('v3f', verts), ('t2f', texc), ('n3f', verts)))







def on_draw():
	fbo = c_uint()
	render_buf = c_uint()
	depth_rb = c_uint()
	glGenFramebuffersEXT(1, byref(fbo))
	glBindFramebufferEXT(GL_DRAW_FRAMEBUFFER, fbo)

	glGenRenderbuffersEXT(1, byref(render_buf))
	glBindRenderbufferEXT(GL_RENDERBUFFER_EXT, render_buf)
	glRenderbufferStorageEXT(GL_RENDERBUFFER_EXT, GL_RGBA8, 800, 800)

	glFramebufferRenderbufferEXT(GL_FRAMEBUFFER_EXT, GL_COLOR_ATTACHMENT0_EXT, GL_RENDERBUFFER_EXT, render_buf)

	glGenRenderbuffersEXT(1, byref(depth_rb))
	glBindRenderbufferEXT(GL_RENDERBUFFER_EXT, depth_rb)
	glRenderbufferStorageEXT(GL_RENDERBUFFER_EXT, GL_DEPTH_COMPONENT24, 800, 800)

	glFramebufferRenderbufferEXT(GL_FRAMEBUFFER_EXT, GL_DEPTH_ATTACHMENT_EXT, GL_RENDERBUFFER_EXT, depth_rb)

	glBindFramebufferEXT(GL_FRAMEBUFFER_EXT, fbo)
	glClearColor(0.0, 0.0, 0.0, 0.0)
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT)

	glViewport(0, 0, 800, 800)
	glMatrixMode(GL_PROJECTION)

	glDisable(GL_TEXTURE_2D)
	glDisable(GL_BLEND)
	glEnable(GL_DEPTH_TEST)

	draw_earth()
	draw_info()
	pixels = (GLubyte * (3 * 800 * 800))(0)
	glReadPixels(0, 0, 800, 800, GL_RGB, GL_UNSIGNED_BYTE, pixels)
	image = Image.frombytes(mode="RGB", size=(800, 800), data=pixels)
	image = image.transpose(Image.FLIP_TOP_BOTTOM)
	image.save('static/img/scr.png', optimize=True)
	print("Saving image to static/img/scr.png")



def draw_earth():
	glLoadIdentity()
	gluPerspective(10, 1, 0.1, 10000.0)
	glMatrixMode(GL_MODELVIEW)
	glLoadIdentity()
	'''glTranslatef(0,0,-zoom+6.37815)'''   # axis rotate by earth's surface
	glRotatef(gyration[1][0], 1, 0, 0) # x-axis rotation
	glRotatef(gyration[1][1], 0, 1, 0) # y-axis rotation
	glRotatef(gyration[1][2], 0, 0, 1) # z-axis rotation
	glTranslatef(0,0,-height-6.37815)
	'''glTranslatef(0,0,-6.37815) '''     # rotate by earth's center
	glRotatef(gyration[0][0], 1, 0, 0)    # latitude rotation
	glRotatef(90-gyration[0][1], 0, 1, 0) # longitude rotation
	glColor3f(1,1,1)
	glEnable(GL_TEXTURE_2D)
	glBindTexture(GL_TEXTURE_2D, earth_texture.id)
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR)
	for v in earth_vlists:
		v.draw(GL_TRIANGLE_STRIP)
	glDisable(GL_TEXTURE_2D)




def draw_info():
	title = pyglet.text.Label("Real-time Satellite Visualization", color=(255,255,255,200))
	'''tname = pyglet.text.Label("Xiaoyu Tai", color=(255,255,255,100))
	ctime = pyglet.text.Label("UTC Time: " + datetime.datetime.utcnow().strftime("%Y-%m-%d %H:%M:%S"), color=(255,255,255,200))'''
	h2 = str(round(height * 1000, 4))
	heightangle = pyglet.text.Label("Satellite height: " + h2 + " km", color=(255,255,255,200))
	'''csate = pyglet.text.Label("Satellites Category: " + resource[num][0], color=(255,255,255,200))'''
	cross = pyglet.graphics.vertex_list(4, ('v3f', [1,0,0,-1,0,0,0,1,0,0,-1,0]))

	glDisable(GL_DEPTH_TEST)
	glLoadIdentity()
	info_draw(title, -6.82, 6.58)
	'''info_draw(tname, 5.32, 6.58)'''
	'''info_draw(ctime, 2.2, -6.8)'''
	info_draw(heightangle, -6.82, -6.8)
	'''info_draw(csate, -6.82, -6.8)'''
	glLoadIdentity()
	glTranslatef(0,0,-80)
	glScalef(0.3, 0.3, 0.3)
	glColor3f(1,1,1)
	cross.draw(GL_LINES)
	glEnable(GL_DEPTH_TEST)

def info_draw(label, x, y):
	glLoadIdentity()
	glTranslatef(x,y,-80)
	glScalef(0.02, 0.02, 0.02)
	label.draw()





init()
on_draw()


