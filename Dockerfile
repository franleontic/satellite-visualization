FROM python:3.10

WORKDIR /app

COPY requirements.txt requirements.txt
RUN pip3 install -r requirements.txt
RUN apt-get update \
	&& apt-get install -y sudo \
	&& sudo apt-get install -y mesa-utils \
	&& sudo apt-get install -y libgles2-mesa-dev

COPY . .

#CMD ["python3", "manage.py", "runserver", "0.0.0.0:$PORT"]

CMD gunicorn satellite_visualization.wsgi --bind 0.0.0.0:$PORT

